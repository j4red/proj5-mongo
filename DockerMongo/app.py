import os, config, logging, flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import acp_times  # Brevet time calculations

# Globals
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

# Step 1: create a client object
# The environment variable DB_PORT_27017_TCP_ADDR is the IP of a linked docker
# container (IP of the database). Generally "DB_PORT_27017_TCP_ADDR" can be set
# to "localhost" or "127.0.0.1".
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

# Step 2: connect to the DB
db = client.acp # database named acp


# Pages
# @app.route('/')
@app.route('/display')
def todo():
    _items = db.brevets.find() # Load collection named brevets
    items = [item for item in _items]
    sorted_items = sorted(items, key=lambda k: k['km'])
    return render_template('display.html', items=sorted_items)

# https://www.diffen.com/difference/GET-vs-POST-HTTP-Requests
@app.route('/new', methods=['POST'])
def new():
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.brevets.insert_one(item_doc)

    return redirect(url_for('display'))


@app.route("/")
@app.route("/index")
def index():
    return flask.render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

# AJAX request handler
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    # app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    selected_dist = request.args.get('selected_dist', 1000, type=int)
    app.logger.debug("selected_dist={}".format(selected_dist))

    initial = request.args.get('initial', type=str)
    app.logger.debug("initial={}".format(initial))

    open_time = acp_times.open_time(km, selected_dist, initial)
    close_time = acp_times.close_time(km, selected_dist, initial)

    result = {
        "open": open_time,
        "close": close_time
    }
    return flask.jsonify(result=result)

@app.route("/_drop", methods=['POST'])
def _drop():
    db.brevets.drop()
    return redirect(url_for('index'))

@app.route("/_add", methods=['POST'])
def _add():
    """ Append entry to MongoDB database. Helped greatly by the discussion at
        https://www.w3schools.com/JQuery/jquery_ajax_get_post.asp. """
    app.logger.debug("New entry received.")
    app.logger.debug(f"form: {request.form}")

    control_doc = {
        'miles': request.form['miles'],
        'km': float(request.form['km']),
        'location': request.form['location'],
        'open': request.form['open'],
        'close': request.form['close'],
        'note': request.form['note'].replace("<br>", "") # remove <br>
    }

    app.logger.debug(f"inserting {control_doc} into db")
    db.brevets.insert_one(control_doc)
    return redirect(url_for('index'))




# Application
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(host='0.0.0.0', port=CONFIG.PORT, debug=True)
