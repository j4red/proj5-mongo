# CIS 322 Project 5: Brevet Time Calculator using MongoDB
Jared Knofczynski, jknofczy@uoregon.edu

## AJAX, Flask, and Mongo reimplementation

This program is a reimplementation of the RUSA Control Time Calculator (https://rusa.org/octime_acp.html) using Flask, AJAX, and MongoDB. Unlike the RUSA calculator, this implementation updates automatically and does not require the form to be submitted each time before viewing the results. Users are also able to submit the entered times into the Mongo database and display the times in order, sorted by distance.

## Using this calculator

To use this calculator, run `docker-compose up` in the `DockerMongo` directory. Afterwards, navigate to `localhost:5000` (or whatever the port is in `DockerMongo/credentials.ini`) in your web browser. From there, select the brevet distance from the dropdown menu, followed by the start date and time for the brevet. The default start time is the current day at 8:00 AM.

Now you may enter the distances of each control point in either the 'Miles' or 'Km' columns on the left. After entering each distance, the open and close times for that control point will be automatically calculated and the fields to the right will be populated. Controls can be adjusted and the open/closing times will be updated automatically.

To ensure proper usage, place your control points in ascending order in either the miles or kilometers columns. Failure to do so will result in a warning in the 'Notes' column. Similarly, if two controls fall on the same distance or one exceeds the specified brevet duration by more than 20%, a warning will occur. A warning will also occur if the control distance exceeds 1000km, as that is the maximum ACP-permitted brevet distance.

To store the current times in the database, click the `Submit` button at the top of the page. All controls currently listed in the calculator will be submitted. If the calculator is empty, the database will be cleared and a warning will occur. Any invalid controls will also incur a warning, and will result in a note being made on the 'Display' page.

To retrieve the controls stored in the database, click `Display`. The controls will be shown in the order of ascending distances (meaning the actual order may differ from that input in the calculator if the controls were not entered properly).

## Calculating the control times

The rules for calculating control times in this program are based on those found at https://rusa.org/pages/acp-brevet-control-times-calculator and https://rusa.org/pages/rulesForRiders. This implementation also uses the French variation discussed in the 'Oddities' section in the first link. The algorithm is as follows:

 | Control Location (km) | Min. Speed (km/hr) | Max. Speed (km/hr)	|
 | :-------------: | :-------------: | :-----------------------:    |
 | 0 - 200  	   | 15  			 | 34							|
 | 200 - 400       | 15  			 | 32							|
 | 400 - 600       | 15  			 | 30							|
 | 600 - 1000      | 11.428  		 | 28							|
 | 1000 - 1300     | 13.333  		 | 26							|

* The distance of each control point (rounded to the nearest KM) is used in calculating the opening and closing times of said control point. The opening and closing delays for a specific control from the starting time are given by `control location / max speed` for that category.

	* A control at 170km would open 170/34 = 5 hours after the race begins, and would close 170/15 = 11.33 = 11 hours and 20 minutes after the starting time.

	* The times for controls in higher categories are given by adding the durations of their smaller parts. For example, a control at 500km in a 600km brevet would open at (200 / 34) + (200 / 32) + (100 / 30) = 15.46 = 15 hours and 28 minutes after the starting time.

* All times are rounded to the nearest minute.

* The French variation indicates that the maximum time limit for a control in the first 60KM is based on 20 KM/hr + 1 hour. After the first 60KM, the standard formula applies.

	* A control point at the beginning of the race (0 KM) will open immediately, and in the French variation, will close after (0 / 20) + 1HR = 1 hour.


* The maximum duration of the brevet depends on its length and is given by the following table.

|  Brevet Length  |    Duration		|
| :-------------: | :-------------: |
| 200 KM		  |	  13.5 hours  	|
| 300 KM		  |	  20.0 hours  	|
| 400 KM		  |	  27.0 hours  	|
| 600 KM		  |	  40.0 hours  	|
| 1000 KM		  |	  75.0 hours  	|


* As this calculator is intended for official ACP brevets only, only 200, 300, 400, 600, and 1000 KM brevets are accounted for.

## Testing

To verify the database functionality of this program is working correctly, click `Submit` with no controls entered in the field. A warning will occur and the database will be cleared (which can be verified by clicking `Display`). After entering in various distances into the control calculator, press 'Submit' to submit the controls. If all is successful, the feedback statement in the top right will indicate that the controls have been submitted. If any errors occur, a warning will appear there instead.

Some specific test cases:

- Submitting controls in any order other than ascending distance should  display a warning message in both the 'Notes' section of the erroneous control, and at the top near the Display button; inputting controls at 50, 100, 75, and 150 km will show the warning at the 75km control. Controls will still be submitted, but will be sorted and displayed in the proper order.

- Pressing Submit with no controls present in the calculator will display a warning indicating that no valid controls were found. This will also clear the database, which you can verify by pressing Display.

- Pressing Display while the database is empty will show a warning on the display page that no controls were found. It will then suggest you return to the calculator and submit new controls.

- Pressing Submit with invalid controls in the calculator will display a warning. Specific examples include controls more than 20% beyond the brevet distance (e.g. controls at 300km in a 200km brevet) or controls in the incorrect order (e.g. controls at 50, 100, then 75km, as mentioned above).

In general, invalid submissions are allowed to be submitted into the database, but will retain any warnings or errors that are detected and will encourage the user to fix the controls in question. Additional tests could include tests that verify clicking Display after submitting valid controls displays the same times as the Calculator page.

A test suite for nosetests is also provided to test the ACP times functionality, and can be run using `nosetests` in the `DockerMongo` directory.

## Contact information
Author: Jared Knofczynski, jknofczy@uoregon.edu  
Program designed for CIS 322 at the University of Oregon in Fall 2020.  
